const {Shop, Item} = require("../src/gilded_rose");

describe("Test unit tests", function() {
  it("test", function() {
    expect(5).toBe(5);
  })
});

describe("Gilded Rose", function() {
  it("should foo", function() {
    const gildedRose = new Shop([new Item("foo", 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("foo");
  });
});

describe("Sulfuras", function() {
  const sulfuras = "Sulfuras, Hand of Ragnaros";

  it("should not change quality", function() {
    const gildedRose = new Shop([new Item(sulfuras, 10, 10)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe(sulfuras);
    expect(items[0].sellIn).toBe(10);
    expect(items[0].quality).toBe(10);
  });
});

describe("Aged Brie", function() {
  const agedB = "Aged Brie";

  it("should increase the quality", function() {
    const gildedRose = new Shop([new Item(agedB, 5, 6)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe(agedB);
    expect(items[0].sellIn).toBe(4);
    expect(items[0].quality).toBe(7);
  });
});

describe("Quality should not be more than 50", function() {
  const agedB = "Aged Brie";

  it("should remain 50", function() {
    const gildedRose = new Shop([new Item(agedB, 5, 50)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe(agedB);
    expect(items[0].sellIn).toBe(4);
    expect(items[0].quality).toBe(50);
  });
});

describe("Quality Should not be negative", function() {
  const tunna = "tunna";

  it("should not be negative", function() {
    const gildedRose = new Shop([new Item(tunna, 1, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe(tunna);
    expect(items[0].sellIn).toBe(0);
    expect(items[0].quality).toBe(0);
  });
});

describe("Quality degrades by two", function() {
  const tunna = "tunna";

  it("should degrade by two", function() {
    const gildedRose = new Shop([new Item(tunna, 0, 5)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe(tunna);
    expect(items[0].sellIn).toBe(-1);
    expect(items[0].quality).toBe(3);
  });
});

describe("Quality increases by two", function() {
  const bp = 'Backstage passes to a TAFKAL80ETC concert';

  it("Quality increases by two", function() {
    const gildedRose = new Shop([new Item(bp, 10, 5)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe(bp);
    expect(items[0].sellIn).toBe(9);
    expect(items[0].quality).toBe(7);
  });
});